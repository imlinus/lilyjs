/* global DOMParser */
/* eslint-disable no-new, no-debugger */
// import compileText from '../utils/compile-text'
// import compileNode from '../utils/compile-node'
import defineReactiveProperties from './../reactive/def-reactive-props'
import * as is from './../utils/is/'
import * as log from './../utils/log/'
import Observer from './../reactive/observer'
import Lily from './../lily'

class Compile {
  constructor (component) {
    Object.assign(this, component)
    this.vm = this
    this.template = this.parse(this.template)
    this.walk(this.template)
    defineReactiveProperties(this)

    if (this.el) this.el.parentNode.replaceChild(this.template, this.el)

    return this.template
  }

  parse (template) {
    const doc = new DOMParser().parseFromString(template.trim(), 'text/xml')
    const documentFragment = document.createDocumentFragment()

    documentFragment.appendChild(doc.documentElement)

    console.log(documentFragment.childNodes[0])
    return documentFragment.childNodes[0]
  }

  walk (node) {
    if (is.elementNode(node)) {
      if (is.customElement(node)) this.compileCustomElement(node)
      else this.compileNode(node)

      node.childNodes.forEach(childNode => {
        this.walk(childNode)
      })
    } else if (is.textNode(node)) {
      this.compileText(node)
    }
  }

  compileText (node) {
    let reg = /{{(.+?)}}/g
    let match

    while ((match = reg.exec(node.nodeValue))) {
      let raw = match[0]
      let key = match[1].trim()
      let data = this.vm.data[key]
      let val = data.value
      let type = data.type

      if (is.type(val, type)) {
        node.nodeValue = node.nodeValue.replace(raw, val)

        new Observer(this.vm, key, (newVal, oldVal) => {
          node.nodeValue = node.nodeValue.replace(oldVal, newVal)
        })
      } else {
        if (type === undefined) {
          log.error({ body: 'You need to provide a "type: ' + val.constructor.name + '" to your data: {} in your <' + this.vm.name + ' /> component.' })
        } else {
          log.error({
            body: val.constructor.name + ' (' + val + ') does not match type: ' + type.name,
            info: 'Please check the data: {} in your <' + this.vm.name + ' /> component.'
          })
        }
      }
    }
  }

  compileNode (node) {
    let attrs = [...node.attributes]

    attrs.forEach(attr => {
      if (this.isModelDirective(attr.name)) {
        this.bindModel(node, attr.value)
      } else if (this.isEventDirective(attr.name)) {
        this.bindEventHandler(node, attr)
      }
    })
  }

  compileCustomElement (node) {
    const name = node.nodeName
    const component = new Compile(Lily.prototype[name])

    node.parentNode.replaceChild(component, node)
  }

  isModelDirective (attr) {
    return attr === 'l-model'
  }

  bindModel (node, attr) {
    new Observer(this.vm, attr, (newVal, oldVal) => {
      node.value = newVal
    })

    node.oninput = (e) => {
      this.vm.data[attr] = e.target.value
    }
  }

  bindEventHandler (node, attr) {
    let eventType = attr.name.substr(5)
    let methodName = attr.value
    node.addEventListener(eventType, this.vm[methodName])
  }

  isEventDirective (attr) {
    return attr.indexOf('l-on') > -1
  }
}

export default Compile
