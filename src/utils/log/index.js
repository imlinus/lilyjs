import error from './error'
import welcome from './welcome'

export {
  error,
  welcome
}
