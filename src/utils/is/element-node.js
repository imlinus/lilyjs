const elementNode = node => {
  return node.nodeType === 1
}

export default elementNode
