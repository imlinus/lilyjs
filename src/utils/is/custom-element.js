const customElement = node => {
  // console.dir(node)
  var HTML_ELEMENTS = [
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hgroup', 'p',
    'br', 'hr', 'img', 'form', 'textarea', 'input', 'label',
    'title', 'meta', 'link', 'script', 'noscript', 'style',
    'html', 'head', 'body', 'div', 'section', 'main',
    'header', 'footer', 'nav', 'ul', 'ol', 'li', 'a',
    'strong', 'b', 'i', 'strike', 'span', 'button'
  ]

  if (!HTML_ELEMENTS.includes(node.tagName)) {
    return true
  }

  return false
}

export default customElement
