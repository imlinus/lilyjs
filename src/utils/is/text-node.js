const textNode = node => {
  return node.nodeType === 3
}

export default textNode
