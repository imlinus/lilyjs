let id = 0

class Subject {
  constructor () {
    this.id = id++
    this.observers = []
  }

  addObserver (observer) {
    this.observers.push(observer)
  }

  removeObserver (observer) {
    let index = this.observers.indexOf(observer)
    if (index > -1) this.observers.splice(index, 1)
  }

  notify (newVal, oldVal) {
    this.observers.forEach(item => {
      item.update(newVal, oldVal)
    })
  }
}

export default Subject
