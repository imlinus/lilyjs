import Lily from './../lily'

const defineReactiveProperties = ({ name, data }) => {
  for (let key in data) {
    Object.defineProperty(Lily.prototype[name], key, {
      enumerable: true,
      configurable: true,
      get () {
        return data[key].value
      },
      set (newVal) {
        data[key].value = newVal
      }
    })
  } // for
} // defineReactiveProperties()

export default defineReactiveProperties
