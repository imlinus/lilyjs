import Subject from './subject'
let currentObserver = null

export function observer (data) {
  if (!data && typeof data !== 'object') return

  for (let key in data) {
    let val = data[key]
    let subject = new Subject()

    Object.defineProperty(data, key, {
      configurable: true,
      enmerable: true,
      get () {
        if (currentObserver) subject.addObserver(currentObserver)

        return val
      },
      set (newVal) {
        subject.notify(newVal, val)
        val = newVal
      }
    })

    if (typeof val === 'object') observer(val)
  } // for
}

class Observer {
  constructor (vm, key, cb) {
    this.vm = vm
    this.key = key
    this.cb = cb
    this.value = this.getVal()
  }

  update (newVal, oldVal) {
    this.cb.bind(this)(newVal, oldVal)
  }

  getVal () {
    currentObserver = this
    var value = this.vm.data[this.key]
    currentObserver = null
    return value
  }
}

export default Observer
