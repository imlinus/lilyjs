/* eslint-disable no-new, no-debugger */
import Compile from './compiler/compile'
import * as log from './utils/log/'

class Lily {
  constructor (opts) {
    log.welcome(this)
    Object.assign(this, opts)
    this.el = document.querySelector(opts.el)
    new Compile(this)
  }

  static component (name, component) {
    component.name = name

    Object.defineProperty(Lily.prototype, name, {
      enumerable: true,
      configurable: true,
      value: component
    })

    return Lily.prototype[name]
  }
}

Lily.config = {
  silent: true,
  prefix: 'l-',
  statements: ['for', 'if', 'else'],
  model: '$',
  event: '@',
  bind: ':'
}

export default Lily
