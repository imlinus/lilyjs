import Lily from './../../src/lily'

const notfound = Lily.component('notfound', {
  data: {
    title: {
      type: String,
      value: '404!'
    },
  },
  template: `
    <div class="404">
      <p>{{ title }}</p>
    </div>
  `
})

export default notfound
