import home from './home'
import notfound from './notfound'

export {
  home,
  notfound
}
