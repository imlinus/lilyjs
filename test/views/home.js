import Lily from './../../src/lily'

const home = Lily.component('home', {
  data: {
    title: {
      type: String,
      value: 'Home!'
    },
    body: {
      type: String,
      value: 'Lorem ipsum dolor sit amet'
    }
  },
  template: `
    <div class="home">
      <h2>{{ title }}</h2>
      <p>{{ body }}</p>
    </div>
  `
})

export default home
