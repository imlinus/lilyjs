import LilyRouter from './../router/'
import {
  home,
  notfound
} from './views/'

const routes = new LilyRouter([
  {
    path: '/',
    name: 'home',
    component: home
  }, {
    path: '*',
    name: '404',
    component: notfound
  }
])

export default routes
