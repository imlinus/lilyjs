/* eslint-disable no-new */
import Lily from './../src/lily'
import routes from './routes'

Lily.config.silent = false

new Lily({
  el: '.app',
  routes,
  template: `
    <div class="app">
      <home />
    </div>
  `
})
